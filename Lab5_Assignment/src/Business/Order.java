/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RAJ_PATIL77
 */
public class Order {
    private ArrayList<OrderItem> orderitemlist;

    public ArrayList<OrderItem> getOrderitemlist() {
        return orderitemlist;
    }

    public void setOrderitemlist(ArrayList<OrderItem> orderitemlist) {
        this.orderitemlist = orderitemlist;
    }

    public int getOrderno() {
        return orderno;
    }

    public void setOrderno(int orderno) {
        this.orderno = orderno;
    }
    private int orderno;
    private static int count=0;
    
    public Order()
    {
        count ++;
        orderno =count;
        orderitemlist = new ArrayList<OrderItem>();
    }
    public OrderItem addOrderItem(Product p,int q,int price)
    {
        OrderItem o =new OrderItem();
        o.setProduct(p);
        o.setQuant(q);
        o.setPrice(price);
        orderitemlist.add(o);
        return o;
    }
    public void removeOrder(OrderItem o){
     orderitemlist.remove(o);
    }
}
