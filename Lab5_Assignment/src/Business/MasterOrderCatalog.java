/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RAJ_PATIL77
 */
public class MasterOrderCatalog {
    private ArrayList<Order> orderCatalog;

    public ArrayList<Order> getOrdercat() {
        return orderCatalog;
    }

    public void setOrdercat(ArrayList<Order> ordercat) {
        this.orderCatalog = ordercat;
    }
    
    public MasterOrderCatalog()
    {
        orderCatalog = new ArrayList<Order>();
    }
    
    public Order addOrder(){
        Order o = new Order();
        orderCatalog.add(o);
        return o;
                
    }
    public void removeOrder(Order o){
        orderCatalog.remove(o);
    }
}
